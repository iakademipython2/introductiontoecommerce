from django.urls import path

from web import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),

    path('customer/<str:id>/', views.customer, name='customer'),
    path('update_customer/<str:id>/', views.updateCustomer, name='update_customer'),
    path('profile/', views.profile, name='profile'),
    path('user_dashboard/', views.user_dashboard, name='user_dashboard'),

    path('products/', views.products, name='products'),
    path('create_order/<str:id>/', views.createOrder, name='create_order'),
    path('update_order/<str:id>/', views.updateOrder, name='update_order'),
    path('delete_order/<str:id>/', views.deleteOrder, name='delete_order'),

    path('signup/', views.signUp, name='sign_up'),
    path('signin/', views.singIn, name='sign_in'),
    path('signout/', views.signOut, name='sign_out'),
    path('reset_password/', auth_views.PasswordResetView.as_view(template_name='reset_password.html'),
         name='reset_password'),

    path('reset_password_done/', auth_views.PasswordResetDoneView.as_view(template_name='reset_password_done.html'),
         name='password_reset_done'),

    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='reset_password_confirm.html'),
         name='password_reset_confirm'),

    path('reset_password_complete',
         auth_views.PasswordResetCompleteView.as_view(template_name='reset_password_complete.html'),
         name='password_reset_complete')

]
