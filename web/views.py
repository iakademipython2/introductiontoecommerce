from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.forms import inlineformset_factory
from django.shortcuts import render, redirect

# Create your views here.
from web.decorators import unauthenticated_user, allowed_users, admin_only
from web.forms import OrderForm, CustomerForm, SignUpUserForm
from web.models import Order, Customer, Product


@login_required(login_url='sign_in')
@admin_only
def dashboard(request):
    customers = Customer.objects.all()
    ordersInfo = Order.objects.all()
    total_orders = ordersInfo.count()
    delivered = ordersInfo.filter(status='Delivered').count()
    pending = ordersInfo.filter(status='Pending').count()
    out_of_delivery = ordersInfo.filter(status='Out for delivery').count()
    orders = Order.objects.order_by('-id')[:5]
    context = {'orders': orders,
               'customers': customers,
               'total_orders': total_orders,
               'delivered': delivered,
               'pending': pending,
               'out_of_delivery': out_of_delivery}
    return render(request, 'dashboard.html', context)

@login_required(login_url='sign_in')
def customer(request, id):
    customer = Customer.objects.get(id=id)
    orders = customer.order_set.all()
    orders_count = orders.count()
    context = {
        'customer': customer,
        'orders': orders,
        'orders_count': orders_count
    }
    return render(request, 'customer.html', context)

@login_required(login_url='sign_in')
def products(request):
    products = Product.objects.all()
    context = {'products': products}
    return render(request, 'products.html', context)

@login_required(login_url='sign_in')
def createOrder(request, id):
    orderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status', 'note'), extra=2)
    customer = Customer.objects.get(id=id)
    formset = orderFormSet(queryset=Order.objects.none(), instance=customer)
    if request.method == 'POST':
        formset = orderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('customer', id)
    context = {'formset': formset}
    return render(request, 'create_order.html', context)

@login_required(login_url='sign_in')
def updateOrder(request, id):
    order = Order.objects.get(id=id)
    form = OrderForm(instance=order)
    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('customer', order.customer.id)
    context = {'form': form}
    return render(request, 'update_order.html', context)

@login_required(login_url='sign_in')
def deleteOrder(request, id):
    order = Order.objects.get(id=id)
    if request.method == 'POST':
        order.delete()
        return redirect('customer', order.customer.id)
    context = {'order': order}
    return render(request, 'delete_order.html', context)

@login_required(login_url='sign_in')
def updateCustomer(request, id):
    customer = Customer.objects.get(id=id)
    form = CustomerForm(instance=customer)
    if request.method == 'POST':
        form = CustomerForm(request.POST, instance=customer)
        if form.is_valid():
            form.save()
            return redirect('customer', id)
    context = {'form': form}
    return render(request, 'update_customer.html', context)


@unauthenticated_user
def signUp(request):
    form = SignUpUserForm()
    if request.method == 'POST':
        form = SignUpUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            group = Group.objects.get(name='customer')
            user.groups.add(group)
            customer = Customer.objects.create(
                user=user,
                name=user.username,
                email=user.email
            )
            print(customer)
            return redirect('sign_in')
    context = {'form': form}
    return render(request, 'signup.html', context)


@unauthenticated_user
def singIn(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dashboard')
        else:
            messages.warning(request, 'Username or Password is incorrect')
    return render(request, 'signin.html')


def signOut(request):
    logout(request)
    return redirect('sign_in')

@login_required(login_url='sign_in')
def profile(request):
    customer = request.user.customer
    form = CustomerForm(instance=customer)
    if request.method == 'POST':
        form = CustomerForm(request.POST, request.FILES, instance=customer)
        if form.is_valid():
            form.save()
            return redirect('profile')
    context = {'form': form}
    return render(request, 'profile.html', context)

@login_required(login_url='sign_in')
@allowed_users(allowed_roles=['customer'])
def user_dashboard(request):
    orders = request.user.customer.order_set.all()
    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()
    out_of_delivery = orders.filter(status='Out for delivery').count()
    context = {
        'orders': orders,
        'total_orders': total_orders,
        'delivered': delivered,
        'pending': pending,
        'out_of_delivery': out_of_delivery
    }
    return render(request, 'user_dashboard.html', context)
